import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author Reshma
 * @Date 6/24/2018
 * @Month 06
 **/
public class CalendarMain {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("resources/applicationContext.xml");
       // Calendar c=(Calendar)ac.getBean("month");
        MonthInfo m=(MonthInfo)ac.getBean("baishak") ;
        m.showCalendar();

    }
}
