import java.sql.SQLOutput;

/**
 * @Author Reshma
 * @Date 6/24/2018
 * @Month 06
 **/
public class MonthInfo {
    final static int CALENDAR_SIZE = 35;
    Calendar c;
    MonthInfo(Calendar c){
        this.c=c;
    }
    void showCalendar(){
        int d= -1;
        int days=c.getDays();
        int gap=c.getGap();
        System.out.println("Sun Mon Tue Wed Thu Fri Sat");
        int a[]=new int[days+gap];
        //System.out.println(a.length);
        for(int i=0;i<a.length;i++){
            if(i<gap){
                a[i]=0;
            }else{
                if(i+1>CALENDAR_SIZE){
                    a[++d] = i-gap+1;
                }else{
                    a[i] = i-gap+1;
                }
            }
        }
        for(int i = 0; i<a.length;i++){
            if(a[i] == 0){
                System.out.print("\t");

            }else{
                System.out.print(a[i]+"\t");
                if ((i+1)% 7 == 0) {
                    System.out.println();
                }
            }
            }
        }
/*        for(int i=0;i<gap;i++)
            System.out.print("\t");

            for (int j = 1; j <= days; j++) {
                System.out.print("\t"+ j);
                if (((j + gap) % 7 == 0) || (j == days))
                    System.out.println();
            }*/



    }

